import logging

# Setup logging as required. Rekall will log to the standard logging service.
logging.basicConfig(level=logging.DEBUG)

import pandas as pd
import re
class DFRender:
    def __init__(self):
        self.df = pd.DataFrame()
        self.using_sections = False
        
        self.maybe_colnames = []
        self.maybe_colvalues = []
        
    def section(self):
        self.using_sections = True
        self.maybe_colnames = []
        self.maybe_colvalues = []
        
    def format(self, *args):
        """This is super hacky and ugly. I built this mainly to work with how dlllist uses the .format() method. Probably not the best way to do this"""
        # First, we only care if args is more than 1
        if len(args) > 1:
            # try to guess some column names, by taking apart what is probably the format string
            self.maybe_colnames += re.split("\{.*?\}", args[0].strip())[0:len(args) - 1]
            self.maybe_colvalues += [str(x) for x in args[1:]]
            
            # Make sure the number of columns and values match
            len_cols = len(self.maybe_colnames)
            len_vals = len(self.maybe_colvalues)
            if len_cols < len_vals:
                for i in range(0, (len_vals - len_cols)):
                    self.maybe_colnames.append("?")
            elif len_vals > len_cols:
                self.maybe_colnames = self.maybe_colnames[0:len_vals]
                
            
        
    def table_header(self, dict_list):
        col_list = []
        if self.using_sections:
            col_list += self.maybe_colnames
        for l in dict_list:
            if isinstance(l, dict):
                if l.has_key('cname'):
                    col_list.append(l['cname'])
                elif l.has_key('name'):
                    col_list.append(l['name'])
            elif isinstance(l, tuple) and len(l) > 2:
                col_list.append(l[1])
            else:
                raise Exception("Don't know how to deal with table_header args %s" % str(dict_list))
                
        self.df = self.df.reindex_axis(col_list, axis=1)
        
    def table_row(self,*args):
        vals = []
        if self.using_sections:
            vals += self.maybe_colvalues
        self.df.loc[len(self.df)] = vals + list(args)