# Rekall and Jupyter/ipython notebook

- rekall_base.ipynb - jupyter notebook
- rekall_base.pdf - a print of the notebook so you can see what it looks like without installing jupyter
- rekdf.py - just the DFRender() class

TODO/notes: Not really happy with the DFRender class. I think a more viable solution, barring full dataframe integration into rekall, perhaps a better solution would be a simple function that you can feed a rekall plugin instance and it has some knowledge about how to build a dataframe for each (some) specific plugin types